require_relative 'lib/octonion'

Gem::Specification.new do |spec|
  spec.name          = "octonion"
  spec.version       = Octonion::VERSION
  spec.authors       = ["Jan Lindblom"]
  spec.email         = ["jan@robotika.ax"]

  spec.summary       = %q{Octonions in Ruby.}
  spec.homepage      = "https://bitbucket.org/janlindblom/ruby-octonion"
  spec.license       = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.3.0")

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://bitbucket.org/janlindblom/ruby-octonion/src/"
  spec.metadata["changelog_uri"] = "https://bitbucket.org/janlindblom/ruby-octonion/src/master/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", ">= 1.14"
  spec.add_development_dependency "rake", "~> 13"
  spec.add_development_dependency "rspec", "~> 3.9"
  spec.add_development_dependency "rubocop", "~> 0.83"
  spec.add_development_dependency "yard", "~> 0.9"
  spec.add_development_dependency "pry", "~> 0.13"
end
