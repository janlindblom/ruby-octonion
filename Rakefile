require "bundler/gem_tasks"
require "rspec/core/rake_task"
require "yard"
require 'rubocop/rake_task'

desc 'Run RuboCop on the lib directory'
RuboCop::RakeTask.new(:rubocop) do |task|
  task.patterns = ['lib/**/*.rb']
  # only show the files with failures
  task.formatters = ['files']
  # DO abort rake on failure
  task.fail_on_error = true
end

RSpec::Core::RakeTask.new(:spec)

YARD::Rake::YardocTask.new do |t|
  t.files   = ['lib/**/*.rb']
  t.stats_options = ['--list-undoc']
end

task :default => [:spec, :yard, :rubocop, :build]

desc 'Prepare a stable version artifact for upload'
task :prepare_prod_artifact => :build do
  require './lib/octonion'
  commit = ENV.key?('BITBUCKET_COMMIT') ? ENV['BITBUCKET_COMMIT'] : 'stable'
  old_name = "pkg/octonion-#{Octonion::VERSION}.gem"
  new_name = "pkg/octonion-#{commit}.gem"
  File.rename(old_name, new_name)
  puts "Renamed #{old_name} to #{new_name}"
end

desc 'Prepare a development artifact for upload'
task :prepare_dev_artifact => :build do
  require './lib/octonion'
  old_name = "pkg/octonion-#{Octonion::VERSION}.gem"
  new_name = 'pkg/octonion-current.gem'
  File.rename(old_name, new_name)
  puts "Renamed #{old_name} to #{new_name}"
end
